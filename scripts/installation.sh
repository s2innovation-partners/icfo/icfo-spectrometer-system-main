wget -qO - https://www.mongodb.org/static/pgp/server-4.2.asc | sudo apt-key add -
echo "deb [ arch=amd64,arm64 ] https://repo.mongodb.org/apt/ubuntu bionic/mongodb-org/4.2 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-4.2.list
sudo apt-get update
sudo apt install -y mariadb-server mariadb-client tango-db tango-common tango-starter tango-test liblog4j1.2-java
sudo apt install -y python-dev python3-dev default-libmysqlclient-dev python-pytango build-essential libboost-dev libboost-all-dev libomniorb4-dev libzmq3-dev libtango-dev
wget -c https://people.debian.org/~picca/libtango-java_9.2.5a-1_all.deb
sudo dpkg -i libtango-java_9.2.5a-1_all.deb
sudo apt -y --fix-broken install
sudo apt install -y mongodb-org nginx nodejs npm
sudo systemctl enable mongod.service
sudo systemctl start mongod.service
pip3 install PyTango
pip3 install pymongo
wget http://ftp.de.debian.org/debian/pool/main/o/openjdk-8/openjdk-8-jre-headless_8u252-b09-1~deb9u1_arm64.deb
sudo dpkg -i openjdk-8-jre-headless_8u252-b09-1_arm64.deb
wget http://ftp.de.debian.org/debian/pool/main/o/openjdk-8/openjdk-8-jdk-headless_8u252-b09-1~deb9u1_arm64.deb
sudo dpkg -i openjdk-8-jdk-headless_8u252-b09-1_arm64.deb
wget http://ftp.de.debian.org/debian/pool/main/o/openjdk-8/openjdk-8-jre_8u252-b09-1~deb9u1_arm64.deb
sudo dpkg -i openjdk-8-jre_8u252-b09-1_arm64.deb
wget http://ftp.de.debian.org/debian/pool/main/o/openjdk-8/openjdk-8-jdk_8u252-b09-1~deb9u1_arm64.deb
sudo dpkg -i openjdk-8-jdk_8u252-b09-1~deb9u1_arm64.deb
