from tango import Database, DbDevInfo
import fandango

db = Database()

dev_info = DbDevInfo()
dev_info.name = 'archiving/snap/manager.1'
dev_info._class = 'SnapManager'
dev_info.server = 'SnapManager/1'
db.add_device(dev_info)

fandango.tango.put_device_property(dev_info.name, "DbHost", "localhost")
fandango.tango.put_device_property(dev_info.name, "DbName", "snap")
fandango.tango.put_device_property(dev_info.name, "DbSchema", "snap")
fandango.tango.put_device_property(dev_info.name, "DbUser", "snapmanager")
fandango.tango.put_device_property(dev_info.name, "DbPassword", "snapmanager")

dev_info = DbDevInfo()
dev_info.name = 'archiving/snap/extractor.1'
dev_info._class = 'SnapExtractor'
dev_info.server = 'SnapExtractor/1'
db.add_device(dev_info)

fandango.tango.put_device_property(dev_info.name, "DbUser", "snapbrowser")
fandango.tango.put_device_property(dev_info.name, "DbPassword", "snapbrowser")

dev_info = DbDevInfo()
dev_info.name = 'archiving/snap/archiver.1'
dev_info._class = 'SnapArchiver'
dev_info.server = 'SnapArchiver/1'
db.add_device(dev_info)

fandango.tango.put_device_property(dev_info.name, "DbHost", "localhost")
fandango.tango.put_device_property(dev_info.name, "DbName", "snap")
fandango.tango.put_device_property(dev_info.name, "DbSchema", "snap")
fandango.tango.put_device_property(dev_info.name, "DbUser", "snaparchiver")
fandango.tango.put_device_property(dev_info.name, "DbPassword", "snaparchiver")
fandango.tango.put_device_property(dev_info.name, "BeansFileName", "beansBeamline.xml")
