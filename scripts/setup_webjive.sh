export SECRET=s3cret
export FRONTEND_URL=http://192.168.0.101

wget -qO - https://www.mongodb.org/static/pgp/server-4.2.asc | sudo apt-key add -
echo "deb [ arch=amd64,arm64 ] https://repo.mongodb.org/apt/ubuntu bionic/mongodb-org/4.2 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-4.2.list
sudo apt install -y mongodb-org
sudo systemctl enable mongod.service
sudo systemctl start mongod.service
sudo apt-get install curl build-essential nginx libboost-dev libboost-all-dev tango-common libtango-dev
pip3 install pytango
curl -sL https://deb.nodesource.com/setup_10.x | bash -
sudo apt-get install -y nodejs

mkdir webjive
cd webjive

git clone https://gitlab.com/s2innovation-partners/icfo/web-maxiv-tangogql
cd web-maxiv-tangogql
git checkout update/raspi
pip3 install -r requirements.txt
cd ..
git clone https://gitlab.com/s2innovation-partners/icfo/dashboard-repo
cd dashboard-repo
git checkout update/raspi
npm i
cd ..
git clone https://gitlab.com/s2innovation-partners/icfo/webjive-auth
cd webjive-auth
git checkout update/raspi
npm i
cd ..
git clone https://gitlab.com/s2innovation-partners/icfo/webjive
cd webjive
git checkout update/raspi
npm i

sudo /etc/init.d/nginx start
