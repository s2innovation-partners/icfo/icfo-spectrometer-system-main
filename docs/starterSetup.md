# Setting up Starter

To run device servers on boot Starter service must be launched during system startup. To do so, please create following scripts:

```
# /etc/systemd/system/tango.target
[Unit]
Description=Tango development environment target
After=mysql.service
Requires=tango-db.service
Requires=tango-starter.service

[Install]
WantedBy=multi-user.target
```

```
# /etc/systemd/system/tango-db.service

[Unit]
Description = Tango DB
Requires=mysql.service
After=mysql.service

[Service]
Restart=on-failure
RestartSec=5
User=pi
Environment=MYSQL_USER=root
#Environment=MYSQL_PASSWORD=
Environment=TANGO_HOST=raspberrypi-web:10000
ExecStart=/usr/lib/tango/DataBaseds 2 -ORBendPoint giop:tcp::10000

[Install]
RequiredBy=tango.target

```

```
# /etc/systemd/system/tango-starter.service
[Unit]
Description=Starter device server
After=tango-db.service
Requires=tango-db.service

[Service]
Restart=always
RestartSec=20
User=pi
Environment=LD_LIBRARY_PATH=/usr/local/lib
Environment=TANGO_HOST=localhost:10000
Environment=ARCHIVING_ROOT=/home/pi/archiving
WorkingDirectory=/home/pi
ExecStart=/usr/local/bin/Starter raspberrypi-web
```

When created, enable all servers to be launched on boot:

```
sudo systemctl enable tango-starter.service
sudo systemctl enable tango-db.service
sudo systemctl enable tango.target
sudo systemctl start tango.target
```

then make sure if server is running:

```
systemctl status tango-starter.service
```

if not, please try to start it manually by executing:

```
systemctl start tango-starter.service
```

if the system will not start check if `Starter` is present under `/usr/local/bin` if not, look for Starter package localization and create an appropiate symbolic link:

```
sudo ln -s /usr/lib/tango/Starter /usr/local/bin/Starter
```

If any device is to be started on system startup, Starter must be also installed on that device connected to tango-host.

```
sudo apt-get install tango-starter
```

Remember about setting TANGO_HOST environment variable on every device connected to tango-host device.
