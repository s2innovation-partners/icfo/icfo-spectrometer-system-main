# Bensikin

Bensikin is used to manage snapshots being saved/restored using [SNAP](../docs/snapSetup.md). Snapshot is based on a context which groups the tango attributes saved in a snapshot.

> Official documentation: https://tango-controls.readthedocs.io/en/latest/tools-and-extensions/archiving/bensikin.html


**Bensikin** allows us to define contexts, take snapshots, and restore them. To run Bensikin from RPi please log in into *TangoHost* RPi and execute the command:

```
/home/pi/archiving/bin/linux/bensikin
```

The following window should pop up:

![BensikinLogin](../img/bensikinLogin.PNG)


Please choose *Ok* to log in.

## Loading available contexts

To load available contexts choose *Contexts/Load/DB*, fill the search fields with desired values, and click search. Some values should load:

![BensikinMainWindow](../img/bensikinMainWindow.PNG)


## Registering new context

To regiser new context run Bensikin in expert mode:

```
/home/pi/archiving/bin/linux/bensikin expert
```

and choose *Contexts/new*. Provide name, author, reason and description then chose attributes to snap. To save context click *Register thie New Context* at the bottom.

When the context is double-clicked, the details load below showing the attributes which are being snapshotted during executing a snapshot. New attributes can also be added thereby selecting the attribute and clicking *arrow* button

On the right side snapshots, history is displayed. When double-clicked on the list, the details of the selected snapshot are displayed and can be restored by clicking *Set Equipments* button.


## Important

Context ID must be provided to webjive when executing *Snap* command from *SnapArchiving* - device *archiving/snap/manager.1 command *LaunchSnapShot*.

## Load data from snapshot

To load attribute's value from snapshot:
1. Select snapshot (red rectangle in the picture below)
2. Select attribute (you can choose more than one value) - green rectangle
3. Click *R -> W* (pink rectangle) - you should see red star next to the snapshot value (pink circle) and the background for Write value change to red
4. Select *Set equipment* (blue rectangle) and click OK


![bensikinLoadProperty](../img/bensikinLoadProperty.jpg)
