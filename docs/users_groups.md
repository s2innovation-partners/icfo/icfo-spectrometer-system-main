# Add users and groups for users

To define user's account (user name, password and group affiliation) we should modify `users.json` file, located in `/home/pi/webjive/users.json` path.

This file contains a dictionary of dictionaries, where the key is user's login and value is a dictionary with password and groups keys.
The value of password key is string and for groups is list (even when we have only one group).

The example of this file:
```python
{
        "user1": {
                "password": "abc123",
                "groups": ["group1", "group2"]
        },
        "user2": {
                "password": "xyz789",
                "groups": ["group3"]
        },
        "icfo1": {
                "password": "icfo1",
                "groups": ["ICFO"]
        },
        "icfo2": {
                "password": "icfo2",
                "groups": ["ICFO"]
        },
        "icfo3": {
                "password": "icfo3",
                "groups": ["ICFO"]
        }
}
```

Based on the above examples, we have 5 users with logins user1, user2, icfo1, icfo2, icfo3. User1 belongs to two groups - group1 and group2 and the rest of the users belong only to one group.

# Apply changes

After modification of `users.json` file, it is necessary to restart raspberry using
```sh
sudo reboot
```
