# Share dashboard
Dashboards can only be shared around a group. All users who will have access to the shared dashboard have to belong to the same group and the current user must be logged in.

To share dashboard:
1. Click the icon in red square
   ![Dashboard](../img/dashboard.png)

2. Expand the **No one** field and select the group with which you want to share the dashboard.
3. If you want to allow users to edit the dashboard mark **Allow the members of the group to edit this dashboard**

4. Chose **Share**