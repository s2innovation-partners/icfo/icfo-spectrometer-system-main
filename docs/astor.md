# Astor 

> The main Astor application documentation: https://tango-controls.readthedocs.io/en/latest/tools-and-extensions/built-in/astor/introduction.html

Astor allows us to have a glance if everything works fine in the system. Astor is connected to TANGO_HOST and shows the status of all devices connected to the system. 

![Astor](../img/astor.png)

Starter device must be configured to run all of the devices added to Astor on system startup. Starter documentation can be found [HERE](starterSetup.md)

To add a new server, manage present ones, or get the information about the description of statuses please refer to the original documentation.
