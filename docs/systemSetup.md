# How to setup all needed packages

## Installation steps:

- run `sh installation.sh`

## Notices

Java in version 8 is required to use Jive application. When installing packages using the installation script please make sure that correct Java version is installed:

```
java -version
```

If not please try to change the currently used Java's version:

```
sudo update-alternatives --config javac
sudo update-alternatives --config java
```

If the Java 8 is not present, try to install it using the instructions provided on the website:
http://www.rpiblog.com/2014/03/installing-oracle-jdk-8-on-raspberry-pi.html

Remember about setting TANGO_HOST environment variable on every device connected to tango-host device.

If mysql password and user are set to custom values, the default tango values can be overwritten in `/etc/tangorc` file by providing variables:

```
MYSQL_USER=
MYSQL_PASSWORD=
```

## Network setup

Each of raspberry pi has an IP address set statically. The configuration can be found in `/etc/network/interfaces` file. Example configuration:

```
allow-hotplug eth0
iface eth0 inet static
        address 10.1.10.6
        netmask 255.255.128.0
        gateway 10.1.10.101
```

Please note that to allow internet access gateway must be set to router's IP address as long as DNS in `/etc/resolv.conf` must be set. For now is set by default to:

```
nameserver 8.8.8.8
```

## SD Card clone

To clone the RPi SD Card [Win32diskimager](https://sourceforge.net/projects/win32diskimager/) application can be used. Once downloaded:

1. Insert the SD Card into the card reader
2. Launch the Win32diskimager and choose the path where the image will be stored as long as the device to read and save the image to PC's drive.
3. Click *Read* - the image is then written to the disk.
4. When finished, remove the SD Card and insert a new one. The new one must be at least the size of the source SD Card or bigger. Please note that different manufacturers' card sizes can differ (even if the same size as promised).
5. Choose the image file written in the previous steps and choose the proper device name.
6. Click *Write* to save the image to the new SD Card.
