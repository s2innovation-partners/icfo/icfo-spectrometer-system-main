# Webjive usage

> Official documantation - https://webjive.readthedocs.io/en/latest/

Webjive can be accessed by providing the RaspberryPi's IP address in the web browser and a database name after `/`, example:

```
http://10.1.10.1/testdb
```

> Please note that if IP address of Raspberry Pi hosting Webjive application will be changed, the environment variable

```
FRONTEND_URL=http://10.1.10.1
```

must also be changes to new IP and device must be restarted.

## Features

Webjive is used to view tango devices using a web browser. It allows to:
- view devices and their attributes,
- execute commands,
- create users. Users management is described in [users and groups](./users_groups.md) docs,
- create, share and save dashboards (dashboards are collections of widgets). Dashboard sharing is described in [share dasboard](./share_dashboards.md) docs,
- visualize device's attributes using a variety of different widgets like scatter plots and heatmaps,
- check the actions performed by users

The *attribute value* widget can change its color depending on the alarms set in Jive. To set alarms please launch Jive on *raspberry-tangohost* and navigate to alarms' tab:

```
Jive -> select the device -> Attribute config -> Alarms tab -> Min/Max Alarm
```

## UI
Main window after user login:

![webjive main window](../img/webjiveMainWindow.PNG)

Dashboard view:

![Webjive dashboard](../img/webjiveDashboard.PNG)

On the right side, available widgets are listed. They can be easily dragged and drop to the dashboard canvas in the middle. Each widget has its attributes, which are being presented after clicking on the desired widget. To choose a device and an attribute to show on the widget please provide appropriate names in the text fields on the left side. If the device name is unknown by the user `*` sign can be inserted to list all the available devices/attributes.

After selecting the attributes, the dashboard can be started by clicking the `Start` button at the top of the screen.

