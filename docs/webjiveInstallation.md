# Webjive installation

- run `sh setup_webjive.sh` - please edit file to provide correct API addresses
- edit `.env` file located in webjive folder. Put the correct API addresses
- execute `npm run build` in webjive directory

> *API address* will be Raspi IP address. Please do not change protocols when changing addresses.

### Notices

`setup_webjive.sh` script sets two environment variables:

```
export SECRET=s3cret
export FRONTEND_URL=http://10.1.10.1
```

If you want to keep them between reboots please put them into the file:

```/etc/environment``` 

## Configure nginx
change `nginx.conf` file located under `/etc/nginx/` to the file provided in repository in `scripts` directory. `root` path provided in configuration file must point to the directory where webjive was build.

## Configure Webjive to start on startup

Create a startup script to run all backend services at once:

```
#!/bin/sh
cd /home/pi/webjive/web-maxiv-tangogql && python3 -m tangogql &
cd /home/pi/webjive/dashboard-repo && npm start &
cd /home/pi/webjive/webjive-auth && node src &
```

Change file permissions:

```sudo chmod a+x startup.sh```

edit `/etc/rc.local` file. At the end, just before `exit 0` put the following line

`su pi -c /home/pi/webjive/startup.sh`


## Configure webjive users 

Edit and copy `user.json` to `/home/pi/webjive` folder. Remember about premissions change:

`chmod a+r users.json`
