# Repositories list

Within the project there are multiple repositories created:

- Main project, containing documentation and installation scripts:
    - [Main system](https://gitlab.com/s2innovation-partners/icfo/icfo-spectrometer-system-main)

- Webjive ([Documentation](webjiveInstallation.md)):
    - [Webjive](https://gitlab.com/s2innovation-partners/icfo/webjive)
    - [web-maxiv-tangogql](https://gitlab.com/s2innovation-partners/icfo/web-maxiv-tangogql)
    - [WebJive Auth](https://gitlab.com/s2innovation-partners/icfo/webjive-auth)
    - [Dashboard Repo](https://gitlab.com/s2innovation-partners/icfo/dashboard-repo)

- Devices:
    - Power Meters([Documentation-3AQUAD](../devices/Ophir3AQUAD.md),[Documentation-PD300](../devices/OphirPd300.md)):
        - [Ophir3AQUADDS](https://gitlab.com/s2innovation-partners/icfo/ophirpowermeterds)
        - [OphirPD300DS](https://gitlab.com/s2innovation-partners/icfo/ophirpd300ds)
        - [OphirPowerMeterLib](https://gitlab.com/s2innovation-partners/icfo/ophirpowermeterlib)

    - Spectrometers([Documentation](../devices/OceanOptics.md)):
        - [OceanOpticsDS](https://gitlab.com/s2innovation-partners/icfo/oceanopticsds)

    - Basler Cameras([Documentation](../devices/Basler.md)):
        - [BaslerDS](https://gitlab.com/s2innovation-partners/icfo/baslerds)

    - Digital Delay Generator ([Documentation](../devices/delayGenerator.md)):
        - [DigitalDelayGenerator](https://gitlab.com/s2innovation-partners/icfo/digitaldelaygenerator)
        - [SocketConnector](https://gitlab.com/s2innovation-partners/icfo/socketconnector)

    - Photodiodes([Documentation](../devices/WieserLabsIPD4.md)):
        - [WieserLabsIPD4BDS](https://gitlab.com/s2innovation-partners/icfo/wieserlabsipd4bds)
        - [WieserLabsIPD4BLib](https://gitlab.com/s2innovation-partners/icfo/wieserlabsipd4blib)

All the repositories are private and hosted on *Gitlab*.

The GNU Lesser General Public License v3.0 is applied to every repository.