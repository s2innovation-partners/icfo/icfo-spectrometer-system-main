# Setting up SNAP and Starter

## Installation steps

- download snap archive https://sourceforge.net/projects/tango-cs/files/tools/ArchivingRoot-16.2.4.zip/download
- create a new directory for extracted files `mkdir /home/pi/archiving`
- extract files `unzip ArchivingRoot-16.2.4.zip`
- replace the `sh` interpreter with `bash` in shebang in scripts located in `bin/linux` and in `device/linux`:

```
sed -i 's/bin\/sh/bin\/bash/g' ./device/linux/**
sed -i 's/bin\/sh/bin\/bash/g' ./bin/linux/**
```

- run SQL script `db/create-SNAPDB-InnoDB.sql` to create the database and users,
```
sudo mariadb < db/create-SNAPDB-InnoDB.sql
```

- set ARCHIVING_ROOT environment variable in `/etc/environment` to point to the directory where *ArchivingRoot* package was extracted

Install fandango:

```
pip3 install fandango
```

Snap devices must be also added to the tango database. To do so, please execute `python3 snap.py`. The script is located in `scripts` directory.

## Setting Snap to be launched on startup

### Starting Snap

To start Snap on boot please open Astor and add appropriate servers. It may be required to change the device paths in Astor. To do so, right click on the desired server, choose `Edit properties` and provide the path where *ArchivingRoot* package was extracted:

```
/home/pi/archiving/device/linux/
```

to run Snap service please make sure if Starter service is running.

```
systemctl status tango-starter.service
```

## Creating Contexts and snapchots

### Bensikin 

to run Bensikin:

```
 /home/pi/archiving/bin/linux/bensikin Expert
```

*Expert* mode is required when one's want to add a new context

To create a snapchot please execute a `LaunchSnapshot` command present in `SnapshotManager` server.
