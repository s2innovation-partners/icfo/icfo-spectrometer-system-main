# Hardware Connection

1. Prepare all the devices to connect.
![](../img/installation/installation_1.jpg)

2. Connect all the raspberries both to switch using ethernet cables, and power supplies. The port to which cable is connected to does not matter - all the devices have a statically assigned IP address.

Please remmember about setting the default gateway in each raspberry. By default it is set to `10.1.10.101` and can be configured by executing:

```
sudo nano /etc/network/interfaces
```

and restarting the device:

```
sudo reboot
```

or single network service
```
sudo /etc/init.d/networking restart
```
![](../img/installation/installation_2.jpg)

3. Prepare the photodiode and raspberry pi #3. Connect the photodiode via USB to the labeled port.

![](../img/installation/installation_3.jpg)
![](../img/installation/installation_4.jpg)

4. Prepare all other devices and connect them using Ethernet to the switch device and power supplies.

![](../img/installation/installation_5.jpg)
![](../img/installation/installation_6.jpg)

> Note: for new devices, it will be necessary to provide IP addresses (or tty path) into *Jive*. To do so, please execute *jive &* command on `raspberrypi-tangohost` device (IP: 10.1.10.2, default user: `pi`, password: `raspberry`), select the desired device and navigate to *properties*. Then in most of the devices *IP* property must be filled end *Simulated* attribute deleted.

5. Power on the whole system.

6. Wait around 1-2 minutes till every device will boot up. 

7. Use the ssh to login to `raspberrypi-tangohost` device (IP: 10.1.10.2, default user: `pi`, password: `raspberry`).

8. Start astor by executing command `astor &` to check if all the devices booted up. When any server is double-clicked, a detailed view is being shown.

![](../img/installation/installation_astor.PNG)

> Note: In case that any of the devices will fail to start (red/orange dot), please start it manually by right click and select `Start device`. If the device will fail to start again, please make sure that all the devices are discovered in the network. To see the status of the devices you can use the `Advanced IP scanner` application to search for the devices available.

9. If everything works as expected, the web browser can be opened providing a raspberrypi-webjive IP address to view webjive application:

```
10.1.10.1/testdb
```