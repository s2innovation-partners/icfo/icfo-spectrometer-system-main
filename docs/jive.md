# Jive

> The main Jive application documentation: https://tango-controls.readthedocs.io/en/latest/tools-and-extensions/built-in/jive/

Jive is an application to browse and edit TANGO database. Is connected to TANGO_HOST and allows us to add/modify/delete/check all devices in the database. Jive also shows the status of all devices connected to the system. 

![Jive](../img/jive.jpg)

To find the information about adding/modifying/deleting/testing devices please refer to the original documentation.

### Add device via Python script

There is also possibility to add device server using Python script.

> Example of Python script
```python
from tango import Database, DbDevInfo

db = Database()
dev_info = DbDevInfo()
dev_info.name = 'spectometer1/OceanOpticsSTS/1'
dev_info._class = 'OceanOpticsSTS'
dev_info.server = 'OceanOpticsSTS/spectrometer1'
db.add_device(dev_info)

db.put_device_property('spectometer1/OceanOpticsSTS/1', {
    'SerialNumber': 'S13999',
    'Simulated': True
})
```
