# Network architecture

The network architecture is shown below:

![Network architecture](../img/icfoNetwork.png)


## Hostname resolving

Each Raspberry Pi has a `/etc/hosts` and `/etc/hostname` file, which allowes to resolve the devices using names instead of ip address.

Example files:

```
#/etc/hosts
127.0.0.1       localhost
::1             localhost ip6-localhost ip6-loopback
ff02::1         ip6-allnodes
ff02::2         ip6-allrouters

127.0.0.1       raspberrypi-tangohost
10.1.10.1       raspberrypi-webjive
10.1.10.4       raspberrypi-basler4
10.1.10.3       raspberrypi-basler3
10.1.10.5       raspberrypi-basler5
10.1.10.6       raspberrypi-otherdevices
```

```
#/etc/hostname 

raspberrypi-tangohost
```
