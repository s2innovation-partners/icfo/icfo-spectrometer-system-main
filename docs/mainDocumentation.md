# ICFO Spectrometer system

## Devices architecture

The system is build using six RaspberryPi devices, each one is used to handle different tasks:

1. Tango host device:
    - Installed software:
        - Jive
        - Astor
        - Tango Controls server
        - Bensikin
2. Webjive host:
    - Installed software
        - Nginx
        - NodeJS
        - Webjive backend and frontend application 
3. Basler 3 - camera 1-2:
    - two Basler camera servers
    - photodiode server
    - Physically connected devices
        - Photodiode using USB
4. Basler 4 - camera 3-4:
    - two Basler camera servers
5. Basler 5 - camera 5-6:
    - two Basler camera servers
6. Other devices host:
    - Physically connected devices:
        - 3x Spectrometr
    - 3x spectromer server
    - 2x delay generator server
    - 6x power meter server

Devices list with IP addresses can be found in [Devices List doc](../devices/devicesList.md).

## Raspberry Pi's
The setup procedure of RasberryPi's is available in documentation [RaspberryPi Setup](./raspberryPiSetup.md). 

All the Raspberries have a default username and password:

```
username: pi
password: raspberry
```

It can be changed using regular Linux commands or using dedicated raspberry pi configuration tool executed using:

```
sudo raspi-config
```

To connect to Raspberry Pi ssh can be used. MobaXTerm application is recommended by us since it also redirects X server to the client which allows us to easily access tools like Jive, Astor, or Bensikin.

To install Tango host on raspberry pi device please follow the instructions provided in [System setup](./systemSetup.md). There can also be found instructions on how to clone SD card.
To install Webjive host on raspberry pi device please follow the instructions provided in [Webjive installation](./webjiveInstallation.md).

Please remmember about setting an appropriate TANGO_HOST environment variable in each Raspberry Pi device.

```
export $TANGO_HOST=tango_host_device_ip:10000
```

## Devices

To install devices please follow the instructions provided in readme of each device repository and instructions provided in docs below. The devices available in the system are as follows:

- 6 Basler cammeras, documented [HERE](../devices/Basler.md), 
- 2 Delay Generators, documented [HERE](../devices/delayGenerator.md),
- 2 Photodiodes, documented [HERE](../devices/WieserLabsIPD4.md),
- 3 Spectrometers, documented [HERE](../devices/OceanOptics.md), 
- 6 Power Meters, [PD300](../devices/OphirPd300.md) and [3AQUAD](../devices/Ophir3AQUAD.md).

To compile the device servers from sources please install the libtango-dev package first:

```
sudo apt install libtango-dev
```

## Devices management

To manage all connected devices and servers, two applications are installed on rasspberrypi-tangohost:

- [Jive](./jive.md)
- [Astor](./astor.md)

To launch them please log into rasspberrypi-tangohost (10.1.10.2) using ssh and execute commands:

```
jive
```

```
astor
```


## Snapchot system

Snapchots are privided by Snap ([How to setup Snap](./snapSetup.md)) and managed using [Bensikin](./bensikin.md).

## Web application

The access to handle devices is provided using *Webjve* and can be accessed using a web browser.

The installation procedure can be found under [Webjive installation](./webjiceInstallation.md).

The usage procedure is described in [Webjive usage procedure](./webjiveUsage.md).

## Network architecture

The network architecture for both RaspberryPi's and connected devices is available in [Network Architecture](./networkArchitecture.md).

## Sources

All the source codes for device drivers and applications are described in [Repositories list](./repositoriesList.md) docs.

### Open-source applications used to build the system

- [Tango Controls](https://www.tango-controls.org/) - software for controlling hardware devices
- [Webjive](https://webjive.readthedocs.io/en/latest/) - web application to assess device's data
- [Astor](https://tango-controls.readthedocs.io/en/latest/tools-and-extensions/built-in/astor/) -  allows us to have a glance if everything works fine in the system. The documentation for the ICFO system can be read [HERE](./astor.md)
- [Bensikin](https://tango-controls.readthedocs.io/en/latest/tools-and-extensions/archiving/bensikin.html) - is used to manage snapshots being saved/restored. The documentation for ICFO system is available [HERE](./bensikin.md)
- [Jive](https://tango-controls.readthedocs.io/en/latest/tools-and-extensions/built-in/jive/) - is used to browse and edit the static TANGO database. The documentation for ICFO system is available [HERE](./jive.md)
