# How to install Tango on Raspberry Pi


## Installation steps:

- ```sudo apt update```
- ```sudo apt install mariadb-server mariadb-client```
- ```sudo apt install tango-db tango-common```
- ```sudo apt install tango-starter tango-test liblog4j1.2-java```
- ```sudo apt install -y python-dev python3-dev default-libmysqlclient-dev python-pytango build-essential libboost-dev libboost-all-dev libomniorb4-dev libzmq3-dev tango-common libtango-dev```
- ```wget -c https://people.debian.org/~picca/libtango-java_9.2.5a-1_all.deb```
- ```sudo dpkg -i libtango-java_9.2.5a-1_all.deb```
- ```sudo apt install openjdk-8-jdk``` (IMPORTANT!: Keep the java version)

## Starting TangoTest

To start TangoTest please type:

```/usr/lib/tango/TangoTest test```