# RaspberryPi setup

## Starting Raspberry

To run the official operating system on RaspberryPi please download it from https://www.raspberrypi.org/forums/viewtopic.php?t=275370. 

> The 64-bit version of OS is still in beta, but it is needed in order to run the newest version of MongoDB.

Once downloaded, use the official SD Card creator to save OS https://www.raspberrypi.org/downloads/

### Use with external monitor and keyboard

RaspberryPi can be used like any other PC. Just connect it using HDMI cable to the external monitor and keyboard using any of the USB ports available.

> microHDMI cable must be used to connect RaspberryPi

### Use without external devices (headless)

By default, ssh is disabled in OS. To enable it, create an `ssh` file inside `boot` partition and leave it empty.

If an Ethernet connection is available, the cable can be plugged directly to Raspberry. WiFi connection can also be enabled, to do so create `wpa_supplicant.conf` file in `boot` partition and fill it with:

```
ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev
update_config=1
country=PL

network={
 ssid="your network SSID"
 psk="password"
}
```

More setup can be done using the dedicated tool accessible by the command `sudo raspi-config`.
