# Digital Delay Generator T560

## Connection

The device can be connected using Ethernet to switch or any other network device. By default static IP address is used `10.1.10.8`. The configuration page can be accessed by typing the IP in the browser URL field.

If the device cannot be found in the network the Device installer app can be downloaded https://www.lantronix.com/products/deviceinstaller/. It allows to search the whole network and list connected devices. To check the Delay Generator IP address, the device can be connected directly to the PC and then search can be performed (IP address should also be set on the PC's network adapter). Device Installer allows also to configure the device.

## Usage

To use the device two devices must be set in Jive: Socket and Delay Generator device

### Socket

Repository: https://gitlab.com/s2innovation-partners/icfo/socketconnector

To run the Socket device please:

1. Clone the repository,
2. execute `make` command. The compiled device is located in `./compiled` folder,
3. add the device to **Jive**, providing the Delay Generator IP address as Socket property,
4. start is by adding it to **Astor** or executing command `./Socket [device name]`.


### Delay Generator device

Repository: https://gitlab.com/s2innovation-partners/icfo/digitaldelaygenerator

To run the Socket device please:

1. Clone the repository,
2. execute `make` command. The compiled device is located in `./compiled` folder,
3. add the device to **Jive**, providing Socket device as `CommunicationProxy` property,
4. start it by adding it to **Astor** or executing command `./DigitalDelayGenerator [device name]`.

Sometimes the Socket device can lost connection. To reconnect it automatically please enable pooling in **Jive** next to `Reconnect` command.

When the device loses physical ethernet connection the server will also be turned off. To turn it back on please check it's status in Astor and restart if needed.

