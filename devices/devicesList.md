# Connected devices list

Each device has an IP address set statically and can be plugged into any of the switch's ports.

- Switch device (Entrasys) - 10.1.10.100 mask 255.255.128.0
- Raspberries:
    - Webjive (raspberrypi-webjive) - 10.1.10.1
    - Tangohost (raspberrypi-tangohost) - 10.1.10.2
    - Basler 3 (raspberrypi-basler3) - 10.1.10.3
    - Basler 4 (raspberrypi-basler4) - 10.1.10.4
    - Basler 5 (raspberrypi-basler5) - 10.1.10.5
    - Other devices (raspberrypi-otherdevices) - 10.1.10.6
- Power meters:
    - PD 300 - 10.1.10.12
    - 3AQUAD - 10.1.10.13
- Basler cameras:
    - Basler 40046228 - 10.1.10.21
    - Basler 40046240 - 10.1.10.20
- Photodiode:
    - WieslerLab - USB
- Digital delay generator:
    - T560 - 10.1.10.8
