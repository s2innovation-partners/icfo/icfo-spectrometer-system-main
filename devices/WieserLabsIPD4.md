# WieserLabs WL-IPD4B

## 1. Installation of WieserLabsIPD4Blib library

To use the WieserLabsIPD4Blib device the library has to be installed.

Repository: https://gitlab.com/s2innovation-partners/icfo/wieserlabsipd4blib

To install library:
1. Clone the repository
2. Execute `python setup.py install`


## 2. Set device

For each WieselLabs device set the device manually in **Jive** or via Python script.

Repository: https://gitlab.com/s2innovation-partners/icfo/wieserlabsipd4bds

To run the device:
1. Clone the repository
2. Please make sure that tango is installed. If not, please follow the installation steps described in [Tango install](../docs/TangoInstallOnRaspberryPi.md) docs
3. Execute `python setup.py install`
4. Add the device, providing:
    - SymLink as Symlink in udev rules, e.g. ```"/dev/ttyIPD4B_Oscillator"```
    - PhotodiodesNos if the number of diodes on the device is different than 3
    - SetPhotodiode - list of commands to set the photodiode, f.e. ```:itm per,:itp 65000 1,:rmask 0x02,:rc```

   ```
   Note the convencion for names:
   device names: photodiode1/WieserLabsIPD4B/1 
   class name: WieserLabsIPD4B
   server name: WieserLabsIPD4B/photodiode1
   ```
      Optional properties:
   - Simulated (Data generation mode, default False)
   - ReadPeriodTau1 (First polling period for reading data, default 1000 ms)
   - ReadPeriodTau2 (Second polling period for reading data, default 10000 ms)

   
5. Change the access permissions of SymLinks, e.g.
    ```shell script
    sudo chmod a+rw /dev/ttyIPD4B
6. Start device by adding it to **Astor** or executing command `WieserLabsIPD4B [name]`

## 3. Creation Symlink for devices

In case of connection troubles, it may be needed to create udev rules in `/etc/udev/rules.d/60-wl_ipd4b.rules`

Example of Symlink for device with idVendor = 0403 and idProduct=6001:
```
SUBSYSTEM=="tty", ATTRS{idVendor}=="0403", ATTRS{idProduct}=="6001", \
ATTRS{product}=="WL-IPD4B", ATTRS{serial}=="AH07F5LF", SUBSYSTEM=="tty", \
GROUP="dialout", SYMLINK+="ttyIPD4B_Oscillator"
```
Note that the whole rule has to be in one line and values for SYMLINK and ATTRS{serial} have to be different for each device.

Next reboot system:
```shell script
sudo reboot
```
