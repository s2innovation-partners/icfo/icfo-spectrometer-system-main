# Power Meter Ophir 3AQUAD

## 1.Connection
The device is connected via Ethernet Adapter. The default static IP address `10.1.10.13` is used. To change the IP configuration read the `Chapter 2 - Configuration:` in mn Ophir EA-1 Ethernet Adapter User Manual.

## 2. Installation of OphirPowerMeterLib  library

To use the Ophir Pd 300 device the library has to be installed.

Repository: https://gitlab.com/s2innovation-partners/icfo/ophirpowermeterlib

To install library:
1. Clone the repository
2. Execute `python setup.py install`


## 3. Set device

For each Ophir 3AQUAD device set the device manually in **Jive** or via Python script.

Repository: https://gitlab.com/s2innovation-partners/icfo/ophirpowermeterds

To run the device:
1. Clone the repository
2. Please make sure that tango is installed. If not, please follow the installation steps described in [Tango install](../docs/TangoInstallOnRaspberryPi.md) docs
3. Execute `python setup.py install`
4. Add the device, providing:
    - IP (the same IP as set for Ethernet Adapter)
   ```
   Note the convencion for names:
   device names: powermeter1/Ophir3AQUAD/1 
   class name: Ophir3AQUAD
   server name: Ophir3AQUAD/powermeter1
   ```
   
   Optional properties:
   - Simulated (Data generation mode, default False)
   - ReadPeriod (Polling period for reading data, default 1000 ms)
   - ReadPoints (Number of collected entries for the Ophir BeamTrack sensor, default 100)
   - NormalizedPoints(Numbers of collected power values, default 1000)
   - MeasurementUnit (Unit of measurements, default "mm" , other option "micron")
   
   
5. Start device by adding it to **Astor** or executing command `Ophir3AQUAD [name]`

Please note that if there is no position or size measurement, value is set to 0 by default.
