# Ocean Optics STS spectrometer

## 1.Connection
The device is connected via USB. 
## 2. Installation of seabreeze library

To use theOptics STS device the seabreeze library has to be installed.


To install library, execute:
```
sudo apt-get install git-all build-essential libusb-dev
pip3 install seabreeze
seabreeze_os_setup
sudo reboot
```

## 3. Set device

For each Optics STS device set the device manually in **Jive** or via Python script.

Repository: https://gitlab.com/s2innovation-partners/icfo/oceanopticsds

To run the device:
1. Clone the repository 
2. Please make sure that tango is installed. If not, please follow the installation steps described in [Tango install](../docs/TangoInstallOnRaspberryPi.md) docs
3. Execute `sudo python3 setup.py install`
4. Add the device, providing:
    - SerialNumber ( the serial number of spectrometer)
   ```
   Note the convencion for names:
   device names: spectometer1/OceanOpticsSTS/1 
   class name: OceanOpticsSTS
   server name: OceanOpticsSTS/spectrometer1
   ```
   
   Optional properties:
   - Simulated (Data generation mode, default False)
   - ReadPeriod (Polling period for reading data, default 1000 ms)
  
   
5. Start device by adding it to **Astor** or executing command `OceanOpticsSTS [name]`
