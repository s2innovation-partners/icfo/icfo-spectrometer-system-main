# Basler camera

## 1. Connection

The device is connected via Ethernet. 

## 2. Installation of required packages

### Prerequirements

Before installing pylon and pypylon please install all needed packages by executing:

```
sudo apt install tango-common libboost-dev libboost-all-dev libtango-dev swig
pip3 install PyTango
```

### Pylon installation

Download the latest pypylon package for your OS from: https://www.baslerweb.com/en/products/software/basler-pylon-camera-software-suite/

Install it using package manager:

```
sudo dpkg -i pylon_5.0.12.11829-deb0_arm64.deb
```

### Pypylon installation

```
git clone https://github.com/basler/pypylon.git
cd pypylon
pip3 install .
```

### Basler camera DS installation

To install device server for Basler camera:
1. Clone the repository
```
git clone https://gitlab.com/s2innovation-partners/icfo/baslerds.git
cd baslerds
```

2. Please make sure that tango is installed. If not, please follow the installation steps described in [Tango install](../docs/TangoInstallOnRaspberryPi.md) docs
3. Execute
```
pip3 install .
```

## 3. Set device

For each Basler camera device set the device manually in **Jive** or via Python script.

Repository: https://gitlab.com/s2innovation-partners/icfo/baslerds


Add the device, providing:
> Note: Before you start using the camera, you must set its static IP, width, height and framerate using Pylon Viewer. When setting *FrameRate* remmember about checking 'Enable Aquisition Frame Rate' checkbox.

- IP (the IP of the Basler camera)
- Width (width of image; suggested value 160)
- Height (height of image; suggested value 120)
- FrameRate (frame rate for Basler camera; suggested value 5)
    > Note: The use of suggested values is recommended since higher values can cause serious performance issues.
   
> Note: It can happen that when eletricity is lost the camera will not store all the values set when restarted. In that case all the values must be set again.

Optional properties:
- Simulated (Data generation mode, default False)
- ReadPeriod (Polling period for reading data, default 1000 ms)

```
Note the convencion for device names:
device names: camera/Basler/1 
class name: Basler
server name: Basler/camera1
```
   
Start device by adding it to **Astor** or executing command `Basler [name]`
